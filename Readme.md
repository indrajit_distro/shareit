# Getting Started

Build the project by running :
mvn package

- Run the microservice:
java -jar shareit-1.0-SNAPSHOT.jar --com.leanapp.shareit.uploadDir=<your path>

where com.leanapp.shareit.uploadDir property refers to the directory where the files will be uploaded

- In order to view the h2 db console run in the dev profile mode:

java -jar shareit-1.0-SNAPSHOT.jar --com.leanapp.shareit.uploadDir=<your path> --spring.profiles.active=dev

url to access h2 db : http://localhost:8000/h2-console

The application is hosted in port : 8000

- Endpoints expected parameters:

POST /api/share
{
	"email":"raj.distro@gmail.com",
	"fileID":"4fd67cd2-4172-4961-9802-feaab1fd499e"
}

POST /api/file
RequestParam name used: file

POST /register
{
	"email":"a.a@a.com",
	"password":"*********"
}

all the endpoints were tested using postman
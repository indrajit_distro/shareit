package com.leanapp.shareit.controller;


import com.leanapp.shareit.model.FileDetails;
import com.leanapp.shareit.model.SharedFileDetails;
import com.leanapp.shareit.model.UserFileDetails;
import com.leanapp.shareit.service.StorageService;
import com.leanapp.shareit.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class FileController implements IFileController {

    private final StorageService storageService;
    private final UserService userService;
    @Autowired
    public FileController(StorageService storageService,UserService userService){
        this.storageService=storageService;
        this.userService=userService;
    }

    @Override
    public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file, Principal principal) {
        String fileID=storageService.store(file,Long.valueOf(principal.getName()));
        return new ResponseEntity<String>(fileID, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Resource> downloadFile(@PathVariable("id") String fileID, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = storageService.load(fileID);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @Override
    public ResponseEntity<String> shareFile(@Valid SharedFileDetails sharedFileDetails, Principal principal) {
        Optional<FileDetails> fileDetails=storageService.shareFile(sharedFileDetails,Long.valueOf(principal.getName()));
        return new ResponseEntity("File shared",HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<UserFileDetails> getFiles(Principal principal) {
        Optional<UserFileDetails> userFileDetails= storageService.getFiles(Long.valueOf(principal.getName()));
        return new ResponseEntity(userFileDetails.get(),HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}

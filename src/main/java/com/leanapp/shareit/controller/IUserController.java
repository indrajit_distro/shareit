package com.leanapp.shareit.controller;

import com.leanapp.shareit.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface IUserController {
    @PostMapping("/register")
    ResponseEntity<String> register(@RequestBody User user);
}

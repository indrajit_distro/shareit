package com.leanapp.shareit.controller;


import com.leanapp.shareit.model.User;
import com.leanapp.shareit.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class UserController implements IUserController{

    @Autowired
    private UserService userService;


    @Override
    public ResponseEntity<String> register(@Valid User user) {
            log.info("inside register method");
            ResponseEntity<String> reponseEntity;
            User duplicateEntry=userService.findUserByEmailId(user.getEmail());
            if(duplicateEntry==null) {
                User userPersisted = userService.saveUserDetails(user);
                if (userPersisted != null) {
                    log.info("email: {} registered successfully", user.getEmail());
                    reponseEntity = new ResponseEntity("User registered successfully",HttpStatus.CREATED);
                } else {
                    reponseEntity = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }else{
                log.info("email: {} already registered", user.getEmail());
                reponseEntity = new ResponseEntity("Email id already used for registration",HttpStatus.CONFLICT);
            }
            return reponseEntity;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}

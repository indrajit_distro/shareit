package com.leanapp.shareit.controller;

import com.leanapp.shareit.model.SharedFileDetails;
import com.leanapp.shareit.model.UserFileDetails;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

public interface IFileController {
    @PostMapping("/file")
    ResponseEntity<String> upload(MultipartFile file, Principal principal);
    @GetMapping("/file/{id}")
    ResponseEntity<Resource> downloadFile(@PathVariable("id") String fileID, HttpServletRequest request);
    @PostMapping("/share")
    ResponseEntity<String> shareFile(@RequestBody SharedFileDetails sharedFileDetails,Principal principal);
    @GetMapping("/file")
    ResponseEntity<UserFileDetails> getFiles(Principal principal);
}

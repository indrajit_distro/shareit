package com.leanapp.shareit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Runtime Exception class for Generic Exception .
 */

@ResponseStatus(HttpStatus.CONFLICT)
public class GenericException  extends RuntimeException {
    public GenericException(String message) {
        super(message);
    }

    public GenericException(String message, Throwable cause) {
        super(message, cause);
    }
}

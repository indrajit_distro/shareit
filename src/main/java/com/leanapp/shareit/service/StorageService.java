package com.leanapp.shareit.service;

import com.leanapp.shareit.configuration.StorageConfiguration;
import com.leanapp.shareit.constants.Status;
import com.leanapp.shareit.exception.FileNotFoundException;
import com.leanapp.shareit.exception.FileStorageException;
import com.leanapp.shareit.exception.GenericException;
import com.leanapp.shareit.model.FileDetails;
import com.leanapp.shareit.model.SharedFileDetails;
import com.leanapp.shareit.model.User;
import com.leanapp.shareit.model.UserFileDetails;
import com.leanapp.shareit.repository.FileRepository;
import com.leanapp.shareit.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: This is a service class , it takes care of storing, retrieving and sharing of the file and getting files wrt user
 */

@Service
@Slf4j
public class StorageService implements IStorageService {
    private final Path fileStorageLocation;
    private final FileRepository fileRepository;
    private final UserRepository userRepository;
    @Autowired
    public StorageService(StorageConfiguration configurationProperties,FileRepository fileRepository,UserRepository userRepository) {
        this.fileStorageLocation = Paths.get(configurationProperties.getUploadDir())
                .toAbsolutePath().normalize();
        this.fileRepository=fileRepository;
        this.userRepository=userRepository;
        try {
            Files.createDirectories(this.fileStorageLocation);

        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }


    @Override
    public String store(MultipartFile file,Long userID) {
        log.info("inside store method");
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            FileDetails fileDetails=new FileDetails();
            fileDetails.setStatus(Status.OWNED);
            fileDetails.setUserID(userID);
            fileDetails.setFilePath(fileName);
            fileDetails=fileRepository.save(fileDetails);
            return fileDetails.getId();
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public Resource load(String fileID) {
        log.info("inside load method");
        Optional<FileDetails> fileDetail=fileRepository.findById(fileID);
            try {
                String filename=fileDetail.get().getFilePath();
                Path filePath = this.fileStorageLocation.resolve(fileDetail.get().getFilePath()).normalize();
                Resource resource = new UrlResource(filePath.toUri());
                if (resource.exists()) {
                    return resource;
                } else {
                    throw new FileNotFoundException("File not found " + filename);
                }
            } catch (MalformedURLException ex) {
                throw new FileNotFoundException("File not found ", ex);
            }catch(NoSuchElementException ex){
                throw new FileNotFoundException("No matching file id found", ex);
            }
    }

    @Override
    public Optional<FileDetails> shareFile(SharedFileDetails sharedFileDetails,Long ownerID) {
        log.info("inside shareFile method");
        Optional<FileDetails> persistedFileDetails = null;
        Optional<FileDetails> fileDetail=fileRepository.findById(sharedFileDetails.getFileID());
        User user= userRepository.findByEmail(sharedFileDetails.getEmail());
        try {
            FileDetails sharedFileDetail = fileRepository.findByBaseFileIDAndUserIDAndStatus(fileDetail.get().getId(), user.getId(), Status.SHARED);
            if (!(ownerID == user.getId())){
                if((ownerID == fileDetail.get().getUserID())&& (fileDetail.get().getStatus() == Status.OWNED))
                {
                    if(sharedFileDetail == null)
                    {
                        FileDetails fileDetails = new FileDetails();
                        fileDetails.setUserID(user.getId());
                        fileDetails.setStatus(Status.SHARED);
                        fileDetails.setFilePath(fileDetail.get().getFilePath());
                        fileDetails.setBaseFileID(fileDetail.get().getId());
                        persistedFileDetails = Optional.ofNullable(fileRepository.save(fileDetails));
                    }else {
                        throw new GenericException("You have already shared this file");
                    }
                }else
                {
                    throw new GenericException("Owner of the file can share this file");
                }
            }else
            {
                throw new GenericException("You are trying to share file with yourself");
            }
        }catch(NoSuchElementException ex)
        {
            throw new FileNotFoundException("No mapping found for the file id",ex);
        }
        return persistedFileDetails;
    }

    @Override
    public Optional<UserFileDetails> getFiles(Long ownerID) {
        log.info("inside getFiles method");
        List<FileDetails> fileDetailsList= fileRepository.findByUserID(ownerID);
        List<FileDetails> ownedList=fileDetailsList.stream().filter(file -> file.getStatus()== Status.OWNED).collect(Collectors.toList());
        List<FileDetails> sharedList=fileDetailsList.stream().filter(file -> file.getStatus()== Status.SHARED).collect(Collectors.toList());
        UserFileDetails userFileDetail=new UserFileDetails();
        userFileDetail.setOwned(ownedList);
        userFileDetail.setShared(sharedList);
        return Optional.of(userFileDetail);
    }
}

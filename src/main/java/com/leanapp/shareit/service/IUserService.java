package com.leanapp.shareit.service;

import com.leanapp.shareit.model.User;
/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Interface to define contracts for the UserService.
 */


public interface IUserService {
    User saveUserDetails(User user);
    User findUserByEmailId(String emailId);
}

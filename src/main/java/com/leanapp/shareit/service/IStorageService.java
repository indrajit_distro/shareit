package com.leanapp.shareit.service;

import com.leanapp.shareit.model.FileDetails;
import com.leanapp.shareit.model.SharedFileDetails;
import com.leanapp.shareit.model.UserFileDetails;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Interface to define contracts for the StorageService.
 */

public interface IStorageService {
    String store(MultipartFile file, Long userID);

    Resource load(String fileID);

    Optional<FileDetails> shareFile(SharedFileDetails sharedFileDetails, Long ownerID);

    Optional<UserFileDetails> getFiles(Long ownerID);
}

package com.leanapp.shareit.service;

import com.leanapp.shareit.model.User;
import com.leanapp.shareit.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: This is a service class , it takes care of persisting, retrieving user details.
 */

@Service
@Slf4j
public class UserService implements IUserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository){
        this.userRepository=userRepository;
    }


    @Override
    public User saveUserDetails(User user) {
        log.info("Inside saveUserDetails method");
        return userRepository.save(user);
    }

    @Override
    public User findUserByEmailId(String emailId) {
        log.info("Inside findUserByEmailId method");
        return userRepository.findByEmail(emailId);
    }



}

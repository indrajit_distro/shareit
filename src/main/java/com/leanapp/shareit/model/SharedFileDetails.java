package com.leanapp.shareit.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Model class used for json --> object requestbody.
 */

@Getter
@Setter
public class SharedFileDetails {
    @NotBlank(message = "email is mandatory")
    private String email;
    @NotBlank(message = "fileID is mandatory")
    private String fileID;
}

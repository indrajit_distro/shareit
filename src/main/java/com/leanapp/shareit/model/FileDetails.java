package com.leanapp.shareit.model;

import com.leanapp.shareit.constants.Status;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Model cum Entity class for storing file details.
 */

@Entity
@Getter
@Setter
public class FileDetails {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String id;  // using UUID as the primary key

    private String filePath;
    private Long userID;
    @Enumerated(EnumType.STRING)
    private Status status;
    private String baseFileID;
}

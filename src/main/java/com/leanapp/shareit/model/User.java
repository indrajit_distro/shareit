package com.leanapp.shareit.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Model cum Entity class used for storing User details.
 */

@Entity
@Getter
@Setter
public class User {
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "email is mandatory")
    private String email;

    @NotBlank(message = "password is mandatory")
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String password;

    public String getPassword() {
        return password;
    }

    // storing encrypted password in database for privacy
    public void setPassword(String password){
        this.password = PASSWORD_ENCODER.encode(password);
    }
}

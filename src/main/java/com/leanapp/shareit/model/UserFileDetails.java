package com.leanapp.shareit.model;

import lombok.Getter;
import lombok.Setter;
import java.util.List;
/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Model class used for files owned by the user or shared to the user ,used for object-->json .
 */

@Getter
@Setter
public class UserFileDetails {
    private List<FileDetails> owned;
    private List<FileDetails> shared;
}

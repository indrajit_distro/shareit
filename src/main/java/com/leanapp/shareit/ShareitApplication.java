package com.leanapp.shareit;

import com.leanapp.shareit.configuration.StorageConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(StorageConfiguration.class)
public class ShareitApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShareitApplication.class, args);
    }

}

package com.leanapp.shareit.constants;

/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Enum to store ownership status of the file.
 */

public enum Status {
    OWNED("OWNED"),
    SHARED("SHARED");

    private final String statusCode;

    Status(String statusCode){
        this.statusCode=statusCode;
    }

    public String getStatusCode(){
        return this.statusCode;
    }
}

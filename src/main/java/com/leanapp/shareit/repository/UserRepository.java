package com.leanapp.shareit.repository;

import com.leanapp.shareit.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 *
 */


@Repository
public interface UserRepository extends CrudRepository<User,Long> {
     User findByEmail(String email);
}

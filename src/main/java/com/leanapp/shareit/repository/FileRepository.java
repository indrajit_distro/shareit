package com.leanapp.shareit.repository;

import com.leanapp.shareit.constants.Status;
import com.leanapp.shareit.model.FileDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 *
 */

@Repository
public interface FileRepository extends CrudRepository<FileDetails,String> {
    FileDetails findByBaseFileIDAndUserIDAndStatus(String baseFileID, Long userID, Status status);
    List<FileDetails> findByUserID(Long userID);
}

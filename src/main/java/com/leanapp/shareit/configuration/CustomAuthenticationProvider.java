package com.leanapp.shareit.configuration;

import com.leanapp.shareit.exception.UserAuthenticationException;
import com.leanapp.shareit.model.User;
import com.leanapp.shareit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;
/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Custom authentication provider .
 */

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @Autowired
    UserService userService;
    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        String emailId = authentication.getName();
        String password = authentication.getCredentials().toString();
        try {
            Optional<User> userDetails = Optional.ofNullable(userService.findUserByEmailId(emailId));
            if (PASSWORD_ENCODER.matches(password, userDetails.get().getPassword())) {
                return new UsernamePasswordAuthenticationToken(
                        userDetails.get().getId(), password, new ArrayList<>());
            } else {
                return null;
            }
        }catch(Exception ex){
            throw new UserAuthenticationException("User not Authorized",ex);
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

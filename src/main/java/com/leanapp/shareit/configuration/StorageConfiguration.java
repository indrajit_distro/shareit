package com.leanapp.shareit.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
/**
 * Created by IntelliJ IDEA.
 * User: indrajit.singh
 * Description: Utility class to map the configuration property for the upload directory .
 */

@ConfigurationProperties("com.leanapp.shareit")
public class StorageConfiguration {
    private String uploadDir;

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }
}
